module.exports = route => {
    route.get('/todos', (req, res) => {
        res.status(200).send({message: 'listing todos'});
    });
    
    route.post('/todos', (req, res) => {
        res.status(201).send({message: 'creating a todo'});
    });
    
    route.put('/todos/:id', (req, res) => {
        const id = req.params.id;
        res.status(200).send({message: `update todo ${id}`});
    });
    
    route.delete('/todos/:id', (req, res) => {
        const id = req.params.id;
        res.status(204).send({message: `delete todo ${id}`});
    });
};